#!/usr/bin/env sh

echo "Starting the SaltStack master"
echo "-----------------------------------"

echo "Cleaning up gitfs locks"
rm -rf /var/cache/salt/master/gitfs/*/.git/update.lk
echo "-----------------------------------"

echo "Fixing permissions"
chmod 700 /home/salt/.ssh
chmod 600 /home/salt/.ssh/*
chown -R salt:salt /home/salt/.ssh
echo "-----------------------------------"

echo "Checking GPG Keyring"
gen-gpg.sh
echo "-----------------------------------"

echo "Checking SSH Keys"
gen-ssh.sh
echo "-----------------------------------"

echo "Running $*"
exec "$@"
