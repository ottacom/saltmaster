# saltmaster

[![Pipeline Status](https://gitlab.com/ix.ai/saltmaster/badges/master/pipeline.svg)](https://gitlab.com/ix.ai/saltmaster/)
[![Docker Stars](https://img.shields.io/docker/stars/ixdotai/saltmaster.svg)](https://hub.docker.com/r/ixdotai/saltmaster/)
[![Docker Pulls](https://img.shields.io/docker/pulls/ixdotai/saltmaster.svg)](https://hub.docker.com/r/ixdotai/saltmaster/)
[![Docker Image Version (latest)](https://img.shields.io/docker/v/ixdotai/saltmaster/latest)](https://hub.docker.com/r/ixdotai/saltmaster/)
[![Docker Image Size (latest)](https://img.shields.io/docker/image-size/ixdotai/saltmaster/latest)](https://hub.docker.com/r/ixdotai/saltmaster/)
[![Gitlab Project](https://img.shields.io/badge/GitLab-Project-554488.svg)](https://gitlab.com/ix.ai/saltmaster/)

Extension of [saltstack/salt:latest](https://hub.docker.com/r/saltstack/salt/)

## Added / Upgraded

* Built-in support for [gitfs](https://docs.saltstack.com/en/latest/topics/tutorials/gitfs.html) using [GitPython](https://github.com/gitpython-developers/GitPython)
* Build-in support for [gpg](https://docs.saltstack.com/en/latest/ref/renderers/all/salt.renderers.gpg.html)
* Always installs the latest [pypi salt](https://pypi.org/project/salt/) version currently available

## Usage

* Use as you normally would [saltstack/salt:latest](https://hub.docker.com/r/saltstack/salt/)
* Mount your SSH private key under `/etc/salt/sshkeys/saltmaster`
* Mount your GPG homedir under `/etc/salt/gpgkeys`

**Note**: If you don't supply the SSH private key or the GPG homedir, they will be automatically generated

### Optionally create the keys manually:

SSH Keys:

```sh
docker volume create sshkeys
docker run --rm --volume sshkeys:/etc/salt/sshkeys registry.gitlab.com/ix.ai/saltmaster gen-ssh.sh
```
GPG Keyring:

```sh
docker volume create gpgkeys
docker run --rm --volume gpgkeys:/etc/salt/gpgkeys registry.gitlab.com/ix.ai/saltmaster gen-gpg.sh
```

## Start it up

```sh
docker run --rm \
           --volume sshkeys:/etc/salt/sshkeys \
           --volume gpgkeys:/etc/salt/gpgkeys \
           --volume cache:/var/cache/salt \
           --volume pki:/etc/salt/pki \
           -p 4505:4505 \
           -p 4506:4506 \
           registry.gitlab.com/ix.ai/saltmaster:latest
```

## Version tags

* `registry.gitlab.com/ix.ai/saltmaster:dev-master` is the latest build on the `master` branch
* `registry.gitlab.com/ix.ai/saltmaster:latest` is the latest build on a git tag
* all other tags follow the salt releases

**Warning**: This image always installs the **latest** [pypi salt](https://pypi.org/project/salt/) version at the time of the pipeline run!

## Multi-Arch Support

Starting with `3005.1`, this image is multi-arch, supporting:

* AMD64
* ARM64
* ARMv7
* ARMv6
* i386

## Resources:

* GitLab: https://gitlab.com/ix.ai/saltmaster
* GitLab Registry: https://gitlab.com/ix.ai/saltmaster/container_registry
* Docker Hub: https://hub.docker.com/r/ixdotai/saltmaster
